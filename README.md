# python_SG

# Python Crash Course - Chapter 3 Summary

****main topics****

```Python
## three lists:
## 1) top 7 most important topics in chapter 3
## 2) top 5 most important
## 3) top 3 most important

top_7 = ['lists', 'methods', 'f-strings', 'append', 'pop', 'sort', 'replace/add/remove from lists']
top_5 = ['lists', 'methods', 'f-strings', 'append', 'sort']
top_3 = ['lists', 'f-strings', 'append']

print(top_7)
print(top_5)
print(top_3)

top_3_msg=f"\n{top_3[0]}\n{top_3[1]}\n{top_3[2]}"

print(top_3_msg)

```

results:
- `['lists', 'methods', 'f-strings', 'append', 'pop', 'sort', 'replace/add/remove from lists']`

- `['lists', 'methods', 'f-strings', 'append', 'sort']`

- `['lists', 'f-strings', 'append']`

# lists

+ ***list*** = collection of items in a particular order
  - enclosed in brackets: `[]`
  - comma separated, text string items in quotes
  - attach to variables like so:

  ```Python
  bicycles = ['trek', 'cannondale', 'redline', 'specialized']
  ```

  if you print like this...

  ```Python
  print(bicycles)
  ```

  ...output is this (yuk!): `['trek', 'cannondale', 'redline', 'specialized']`

  do this instead!:

  ```Python
  print(bicycles[0])
  ```

  output is: `trek`

    `[0]` means 'do something with the FIRST item in the ***list*** ('trek' in this case)

    `[1]` indicates the SECOND item in the ***list*** ('cannondale')

    ... and so on


# methods

* modify the print output like this

    ```Python
    print(bicycles[0].title())
    ```

    adding `.title()` makes the output be this:

    output: `Trek`

    ^^^ automatically capitalized

    or:

    ```Python
    print(bicycles[0].upper())
    ```

    output: `TREK`

    ^^^ALL UPPERCASE

    `.title()` and `.upper()` are ***methods***, and there are many


# f-strings

  - a way to use ***list*** items or variables in a sentence:

    ```Python
    message=f"My first bicycle was a {bicycles[0].title()}."
    ```

    the `f` in the above command (just before the first `"`) is what allows us to insert variables or list items inside the quote (these are called `f-strings`)

    ```Python
    print(message)
    ```

    output:
    `My first bicycle was a Trek.`

    without ***f-strings***, the above output would look like this:

    output: `My first bicycle was a {bicycles[0].title()}.
    `


# replace, add, remove items in a list


- **replace:**

   start with this list:

   ```Python
    motorcycles = ['honda', 'yamaha', 'suzuki']
    print(motorcycles)
   ```

   output: `['honda', 'yamaha', 'suzuki']`

   we can replace 'honda' with 'ducati' like so:
   ```Python
   motorcycles[0] = 'ducati'
   print(motorcycles)
   ```

  output: `['ducati', 'yamaha', 'suzuki']`

- **append:**

  ```Python
   motorcycles.append('corvette')
   print(motorcycles)
  ```

  output: `['ducati', 'yamaha', 'suzuki', 'corvette']`

  start with an empty list, build it up with append:

  ```
  motorcycles = []
  motorcycles.append('honda')
  motorcycles.append('yamaha')
  motorcycles.append('suzuki')
  print(motorcycles)
  ```

  output: `['honda', 'yamaha', 'suzuki']`

- **insert:**

  ```Python
  motorcycles = ['honda', 'yamaha', 'suzuki']
  motorcycles.insert(0, 'ducati')
  print(motorcycles)

  motorcycles.insert(1, 'harley-davidson')
  print(motorcycles)

  ```

  output: ['ducati', 'honda', 'yamaha', 'suzuki'] `

- **delete:** del command removes a value permanently, value cannot be accessed again

  ```Python
  motorcycles = ['honda', 'yamaha', 'suzuki']
  del motorcycles[0]
  print(motorcycles)
  ```

  output: `['yamaha', 'suzuki']`

- **pop:**  removes the last item in a list, but it lets you work
with that item after removing it

  ```Python
  popped_motorcycle=motorcycles.pop()
  print(popped_motorcycle)
  ```
  output: `suzuki`

  ^^^ the following prints the same thing as above:  print(motorcycles.pop())

  ```Python
  print(motorcycles)
  ```

  this is what's left in the `orig motorcycles` list
  output: `['yamaha']`

  pop out something other than the last value:

  ```Python
  motorcycles = ['honda', 'yamaha', 'suzuki']
  first_motorcycle=motorcycles.pop(0)
  message=f"my first motorcycle was a {first_motorcycle.title()}"
  print(message)
  ```

  output: `my first motorcycle was a Honda`

- **remove:** remove an item from a list by name (not by position in the list)

  ```Python
  motorcycles = ['honda', 'yamaha', 'suzuki', 'ducati']
  print(motorcycles)
  ```

  output: `['honda', 'yamaha', 'suzuki', 'ducati']`

  ```Python
  too_expensive='ducati'
  motorcycles.remove(too_expensive)
  print(f"\nA {too_expensive.title()} is too expensive for me!")
  ```

  output: `A Ducati is too expensive for me!`

  ```Python
  print(motorcycles)
  ```

  output: `['honda', 'yamaha', 'suzuki']`

  ^^^ ducati is removed from the list


---

# EXERCISE:

- **lab 3-4 thru 3-7 exercise:**


> The following exercises are a bit more complex than those in Chapter 2, but
they give you an opportunity to use lists in all of the ways described.

> 3-4. Guest List: If you could invite anyone, living or deceased, to dinner, who
would you invite? Make a list that includes at least three people you’d like to
invite to dinner. Then use your list to print a message to each person, inviting
them to dinner.

> 3-5. Changing Guest List: You just heard that one of your guests can’t make the
dinner, so you need to send out a new set of invitations. You’ll have to think of
someone else to invite.

> •	 Start with your program from Exercise 3-4. Add a print() call at the end
of your program stating the name of the guest who can’t make it.

> •	 Modify your list, replacing the name of the guest who can’t make it with
the name of the new person you are inviting.

> •	 Print a second set of invitation messages, one for each person who is still
in your list.

> 3-6. More Guests: You just found a bigger dinner table, so now more space is
available. Think of three more guests to invite to dinner.

> •	 Start with your program from Exercise 3-4 or Exercise 3-5. Add a print()
call to the end of your program informing people that you found a bigger
dinner table.

> •	 Use insert() to add one new guest to the beginning of your list.

> •	 Use insert() to add one new guest to the middle of your list.

> •	 Use append() to add one new guest to the end of your list.

> •	 Print a new set of invitation messages, one for each person in your list.

> 3-7. Shrinking Guest List: You just found out that your new dinner table won’t
arrive in time for the dinner, and you have space for only two guests.

> •	 Start with your program from Exercise 3-6. Add a new line that prints a
message saying that you can invite only two people for dinner.

> •	 Use pop() to remove guests from your list one at a time until only two
names remain in your list. Each time you pop a name from your list, print
a message to that person letting them know you’re sorry you can’t invite
them to dinner.

> •	 Print a message to each of the two people still on your list, letting them
know they’re still invited.

> •	 Use del to remove the last two names from your list, so you have an empty
list. Print your list to

remember:
 - putting on `f` in front of a text string allows you to insert variables in the text string
    - ex: `print(f"hello {variable}, how are you?")`

 - printing the results of your work to the console is a good way to double-check your make



# ANSWER_KEY

    ```Python
    ## 3-4
    guest_list=['bob','tom','cindy']

    message1=f"Dear {guest_list[0].title()}, come to my thang!"
    message2=f"Dear {guest_list[1].title()}, come to my thang!"
    message3=f"Dear {guest_list[2].title()}, come to my thang!"

    print(message1)
    print(message2)
    print(message3)

    ## 3-5

    cant_make_it=guest_list[1]

    print(f"My bestest bud {cant_make_it.title()}, can't make it! I'm devastated, need to find a replacement")
    print(guest_list)

    guest_list[1]=('bufford')
    print(guest_list)

    print(f"{guest_list[1].title()}, my new bestest bud, has been added to the guest list!")

    message1=f"Dear {guest_list[0].title()}, come to my thang!"
    message2=f"Dear {guest_list[1].title()}, come to my thang!"
    message3=f"Dear {guest_list[2].title()}, come to my thang!"

    print(message1)
    print(message2)
    print(message3)

    ## exercise 3-6

    guest_list=['bob','tom','cindy']
    guest_list.insert(0, 'bufford')
    guest_list.insert(1, 'heather')
    guest_list.append('tina')

    print(guest_list)

    message1=f"Dear {guest_list[0].title()}, bigger table = bigger party! More invites"
    message2=f"Dear {guest_list[1].title()}, bigger table = bigger party! More invites"
    message3=f"Dear {guest_list[2].title()}, bigger table = bigger party! More invites"
    message4=f"Dear {guest_list[3].title()}, bigger table = bigger party! More invites"
    message5=f"Dear {guest_list[4].title()}, bigger table = bigger party! More invites"
    message6=f"Dear {guest_list[5].title()}, bigger table = bigger party! More invites"

    print(message1)
    ### etc....

    ## 3-7
    guest_list=['bufford', 'heather', 'bob', 'tom', 'cindy', 'tina']

    print(f"\nOh no! {guest_list[0].title(), guest_list[1].title(), guest_list[2].title(), guest_list[3].title(), guest_list[4].title(), guest_list[5].title()}, \nan unexpected thing happened!\nOnly room for 2 guests now!")

    print(guest_list)

    hard_truth=", \nI'm so sorry, only 2 guests can now come. It's a long story I can't go into right now.\nBut the hard truth is this:\nYou are not quite important enough in my life to make the cut.\n"

    bufford_message=f"Dear {guest_list.pop(0).title()}{hard_truth}"
    heather_message=f"Dear {guest_list.pop(0).title()}{hard_truth}"
    tom_message=f"Dear {guest_list.pop(1).title()}{hard_truth}"
    tina_message=f"Dear {guest_list.pop().title()}{hard_truth}"

    print(bufford_message)
    print(heather_message)
    print(tom_message)
    print(tina_message)

    print(guest_list) ## should only be two names left

    ## delete both:
    del guest_list[0]
    del guest_list[0]
    print(guest_list) ## list should now be empty
    ```

# Shorter_Answer_Key

```Python

## 3-4 thru 3-7

dinner_guests=['gandhi','malcolm x','jfk']

print(dinner_guests)

dinner_guests.append('keanu reeves')

print(dinner_guests)

invite_msg=f"Hi {dinner_guests}, you're invited!"

print(invite_msg)

private_msg=f"{dinner_guests[-1].title()} & {dinner_guests[1].title()}, If you guys don't like them I'll disinvite them I guess..."

print(private_msg)

dinner_guests.sort()

print(dinner_guests)

blow_off_msg=f"Oh hey {dinner_guests.pop(0).title()} & {dinner_guests.pop(0).upper()}, something came up and uh...."

print(blow_off_msg)
print(dinner_guests)

del dinner_guests[0]
del dinner_guests[0]

print(dinner_guests)
```

# Shortest_Answer_Key

```Python
## 3-4 thru 3-7
dinner_guests=['gandhi','malcolm x','jfk']
print(dinner_guests)
dinner_guests.append('keanu reeves')
print(dinner_guests)
invite=f"Hi {dinner_guests}, you're invited!"
print((invite))
blow_off_msg=f"Hey {dinner_guests.pop(0).title()} & {dinner_guests.pop(1).upper()}, I hate to say it but, dinner cancelled"
print(blow_off_msg)
del dinner_guests[0]
del dinner_guests[0]
print(dinner_guests)
```
---

# Organizing a list

- **sort:** permanently sort a list (alphabetical, numerical, dates) - default is alphanumeric

  ```Python
  cars = ['bmw', 'audi', 'toyota', 'subaru']
  cars.sort()
  print(cars)
  ```

  output: `['audi', 'bmw', 'subaru', 'toyota']`

- **reverse sort:** Same as above, but reverse!

  ```Python

  cars = ['bmw', 'audi', 'toyota', 'subaru']

  ## sort

  cars.sort()
  print(cars)

  ##reverse sort
  cars.sort(reverse=True)
  print(cars)

  ```

- **sorted:** temporarily sort a list (displays sorted but orig list stays as is)

  ```Python
  cars=['bmw','audi','toyota','subaru']
  print("\nHere's the original:")
  print(cars)

  print("\nHere's sorted:")
  print(sorted(cars))

  print("\nAaaand orig is unchanged:")
  print(cars)

  ```
